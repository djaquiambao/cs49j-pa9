////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/17/2020
//  Description: This program demonstrates simple and basic functionalities for a savings account
//  Including a calculate button which calculates the account savings using Interest Rate Formula
//  for the time variable as STRICTLY years (integer).
///////////////////////////////////////
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//Helper Class for storing account info
class SavingsAccount
{
    private double dSavingsAmount;
    private double dAPR;
    private int nYears;

    public SavingsAccount()
    {
        this.dSavingsAmount = 0;
        this.dAPR = 0;
        this.nYears = 0;
    }

    //Calculator containing formula for Interest Rate.
    public double calculate(double balance, double rate, int years)
    {
        return balance*(1 + (rate/100)*years);
    }

    //ACCESSORS
    public double getdSavingsAmount()
    {
        return dSavingsAmount;
    }
    public double getdAPR()
    {
        return dAPR;
    }
    public int getnYears()
    {
        return nYears;
    }
}

class AccountViewer extends JFrame
{
    //Attributes
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 300;
    private JLabel rateLabel, accountLabel, yearLabel, resultLabel;
    private JTextField rateField, accountField, yearField;
    private double balance;
    private final double DEFAULT_RATE = 9.0, INITIAL_BALANCE = 0.0;
    private final int YEAR_INIT = 1;
    private SavingsAccount savingsAcct;
    private JButton calculateButton, clearButton;
    private JPanel panel, buttonPanel;


        //Constructor
    public AccountViewer()
        {
            ////CREATE TEXT FIELDS/////
            panel = new JPanel();
            panel.setLayout(new GridLayout(10, 2));
            accountLabel = new JLabel("Deposit Amount: ");
            rateLabel = new JLabel("Annual Interest Rate (%): ");
            yearLabel = new JLabel("Year: ");

            final int FIELD_WIDTH = 10;
            accountField = new JTextField(FIELD_WIDTH);
            accountField.setText("" + INITIAL_BALANCE);

            rateField = new JTextField(FIELD_WIDTH);
            rateField.setText("" + DEFAULT_RATE + "%");

            yearField = new JTextField(FIELD_WIDTH);
            yearField.setText("" + YEAR_INIT);
            resultLabel = new JLabel("Balance: " + balance);

            panel.add(accountLabel);
            panel.add(accountField);
            panel.add(rateLabel);
            panel.add(rateField);
            panel.add(yearLabel);
            panel.add(yearField);
            panel.add(resultLabel);

            ///// CREATE BUTTON /////

            //Calculate Button//
            calculateButton = new JButton("Calculate");
            ActionListener calculateListener = new CalculateListener();
            calculateButton.addActionListener(calculateListener);
            //Clear Button//
            clearButton = new JButton("Clear");
            ActionListener clearListener = new ClearListener();
            clearButton.addActionListener(clearListener);

            buttonPanel = new JPanel();
            buttonPanel.setLayout(new FlowLayout());
            buttonPanel.add(calculateButton);
            buttonPanel.add(clearButton);

            /////////////////////////
            //////////INITIALIZE MAIN FRAME/////////////

            JPanel mainPanel = new JPanel();

            savingsAcct = new SavingsAccount();
            balance = savingsAcct.calculate(savingsAcct.getdSavingsAmount(), savingsAcct.getdAPR(), savingsAcct.getnYears());
            mainPanel.setLayout(new BorderLayout());
            mainPanel.add(panel, BorderLayout.CENTER);
            mainPanel.add(buttonPanel, BorderLayout.SOUTH);
            add(mainPanel);
            setSize(FRAME_WIDTH, FRAME_HEIGHT);
            setTitle("Savings Account Calculator");
        }
        //Listener for Calculate Button
        class CalculateListener implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double account = Double.parseDouble(accountField.getText());
                    String[] strSplit = rateField.getText().split("%");
                    double rate = Double.parseDouble(strSplit[0]);
                    int year = Integer.parseInt(yearField.getText());
                    rateField.setText(rate + "%");
                    balance += savingsAcct.calculate(account, rate, year);
                    resultLabel.setText("Balance: " + String.format("%.3f", balance));
                }
                catch(NumberFormatException | IndexOutOfBoundsException n)
                {
                    n.getStackTrace();
                }
            }
        }

        //Listener for Clear Button
        class ClearListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                balance = 0;
                resultLabel.setText("Balance: " + balance);
            }
        }

}

public class P20_3 {
    public static void main(String[] args) {
        JFrame savingsAccount = new AccountViewer();
        savingsAccount.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        savingsAccount.setVisible(true);
    }
}

////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/15/2020
//  Description: This program generates more or fewer rectangles and starts with 6. It features a Menu drop down=
//  with menu items. More generates double of the rectangles for each method call, while fewer shrinks the number down
//  to half. It features ArrayList to keep track of the number of Rectangles and remove them upon user call/interaction.
///////////////////////////////////////
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

class RectGen_E20_7 extends JComponent
{
    private Rectangle rect;
    //Keeps count of the number of rectangles.
    private int numRect;
    //MAXIMUM Points (X/Y) for rectangle.
    private final int MAX_WIDTH = 600, MAX_HEIGHT = 400;
    //Using arrayList, we can keep track of the rectangles generated, and add or remove correspondingly.
    private ArrayList<Rectangle> rectangles;
    //Random number generated to randomly generate positions for rectangles.
    private Random rNum = new Random();
    //Constructor to generate Rectangles
    public RectGen_E20_7(int numRect)
    {
        this.numRect = numRect;
        rectangles = new ArrayList<>();
        for(int i = 0; i < numRect; i++)
        {
            rect = new Rectangle(rNum.nextInt(MAX_WIDTH), rNum.nextInt(MAX_HEIGHT), 40, 30);
            rectangles.add(rect);
        }
    }
    public void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        try {
            if(numRect > 0) {
                for (int i = 0; i < numRect; i++) {
                    g2.draw(rectangles.get(i));
                }
            }
        }
        catch(IndexOutOfBoundsException e)
        {
            e.getStackTrace();
            System.out.println("Index out of bounds exception caught!");
        }
    }
    //Method to generate fewer Rectangles.
    public void fewer()
    {
        try{
            if(numRect > 1) {
                numRect /= 2;
                for (int i = 0; i < numRect; i++) {
                    rectangles.remove(i);
                }
            }
            else {
                numRect = 1;
            }

            repaint();
        }
        catch(IndexOutOfBoundsException o)
        {
            o.getStackTrace();
            System.out.println("Index out of bounds exception caught!");
        }
    }
    public void more()
    {
        numRect*=2;
        for(int i = 0; i < numRect; i++)
        {
            rect = new Rectangle(rNum.nextInt(MAX_WIDTH), rNum.nextInt(MAX_HEIGHT), 40, 30);
            rectangles.add(rect);
        }
        repaint();
    }
}

class RectView_E20_7 extends JFrame
{
    //JFrame width and height
    final int WIDTH = 800, HEIGHT = 600;
    //Declare a RectangleGenerator object.
    private RectGen_E20_7 rects;

    //Listener for Fewer MenuItem
        class Fewer implements ActionListener
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                //calls method fewer to
                rects.fewer();
            }
        }

        //Listener for More MenuItem
        class More implements ActionListener
        {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Calls the method more from object rect
                rects.more();
            }
        }

        //Constructor
    public RectView_E20_7()
    {
        rects = new RectGen_E20_7(6);
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        JMenu options = new JMenu("Options");
        JMenuItem fewer = new JMenuItem("Fewer");
        JMenuItem more = new JMenuItem("More");
        options.add(fewer);
        options.add(more);
        menuBar.add(options);
        //Instantiate Listeners
        ActionListener fewerListener = new Fewer();
        ActionListener moreListener = new More();
        //Install listeners
        fewer.addActionListener(fewerListener);
        more.addActionListener(moreListener);
        setSize(800, 600);
        setTitle("Rectangle Generator");
        add(rects);
    }
}

public class E20_7 extends JFrame{

    public static void main(String[] args) {

        JFrame frame = new RectView_E20_7();

        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

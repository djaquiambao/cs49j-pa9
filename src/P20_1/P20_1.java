////////////////////////////////////////
//  Author: Darwish Quiambao
//  Course Number: CS49J
//  Date Created: 11/15/2020
//  Description: This program expands on E20_7 and features sliders instead of MENU ITEMS
///////////////////////////////////////
package P20_1;

import javax.swing.*;

//Driver Program
public class P20_1 extends JFrame{

    public static void main(String[] args) {

        JFrame frame = new RectangleViewer();

        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

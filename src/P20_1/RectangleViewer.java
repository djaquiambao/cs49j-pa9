package P20_1;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

//Sets up the Frame for viewing rectangles
public class RectangleViewer extends JFrame {
    //JFrame width and height
    final int WIDTH = 800, HEIGHT = 600;
    //Declare a E20_7.RectangleGenerator object.
    private RectangleGenerator rects;

    //Listener for Fewer MenuItem
    class Rectangles implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            //calls method fewer to
            JSlider slider = (JSlider) e.getSource();
            rects.rectangleSlider(slider.getValue());
        }
    }

    //Constructor
    public RectangleViewer() {
        //Instantiate Listeners
        ChangeListener listener = new Rectangles();
        //Install listeners
        JSlider rectSlider = new JSlider(JSlider.HORIZONTAL, 0, 20, 10);
        //Configure the Slider
        rectSlider.setMajorTickSpacing(10);
        rectSlider.setMinorTickSpacing(1);
        rectSlider.setPaintTicks(true);
        rectSlider.setPaintLabels(true);
        rectSlider.setPaintTrack(true);
        rectSlider.setSnapToTicks(true);
        rectSlider.addChangeListener(listener);
        //Instantiate a rectangle Generator object
        rects = new RectangleGenerator(rectSlider.getValue());
        JPanel sliderPanel = new JPanel();
        //sliderPanel.setLayout(new GridLayout(1,2));
        sliderPanel.add(new JLabel("Rectangles"));
        sliderPanel.add(rectSlider);

        setSize(800, 600);
        setTitle("Rectangle Generator");
        add(rects, BorderLayout.CENTER);
        add(sliderPanel, BorderLayout.NORTH);
    }
}
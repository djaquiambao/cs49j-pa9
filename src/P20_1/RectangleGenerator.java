package P20_1;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class RectangleGenerator extends JComponent {
    private Rectangle rect;
    //Keeps count of the number of rectangles.
    private int numRect;
    //MAXIMUM Points (X/Y) for rectangle.
    private final int MAX_WIDTH = 600, MAX_HEIGHT = 400;
    //Using arrayList, we can keep track of the rectangles generated, and add or remove correspondingly.
    private ArrayList<Rectangle> rectangles;
    //Random number generated to randomly generate positions for rectangles.
    private Random rNum = new Random();
    private JPanel panel;

    //Constructor to generate Rectangles
    public RectangleGenerator(int numRect) {

        panel = new JPanel();
        rectangles = new ArrayList<>();
        this.numRect = numRect;

        for (int i = 0; i < this.numRect; i++) {
            rect = new Rectangle(rNum.nextInt(MAX_WIDTH), rNum.nextInt(MAX_HEIGHT), 40, 30);
            rectangles.add(rect);
        }
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        try {
            //Draw the ArrayList of rectangle objects.
                for (Rectangle rectangle : rectangles) {
                    g2.draw(rectangle);
            }
        } catch (IndexOutOfBoundsException e) {
            e.getStackTrace();
            System.out.println("Index out of bounds exception caught!");
        }
    }

    public void rectangleSlider(int sliderVal)
    {
        //assign the size of arraylist to numRect attribute
        numRect = rectangles.size();
        try {
            //check if the slidervalue from JSlider is greater than the CURRENT number of Rectangles in ArrayList
            if (sliderVal > numRect)
            {
                //if True, add new Rectangle Object into arrayList
                //subtracting the sliderValue from the current array list ensures it only adds the right amount (1)
                for (int i = 0; i < (sliderVal-numRect); i++) {
                    rectangles.add(new Rectangle(rNum.nextInt(MAX_WIDTH), rNum.nextInt(MAX_HEIGHT), 40, 30));
                }
            }
            //Check if sliderValue is less than the number of rectangles in array list
            else if(sliderVal < numRect){
                //remove the number of rectangles that is the DIFFERENCE of sliderValue and CURRENT number of rectangles
                for(int i = 0; i < (numRect - sliderVal); i++)
                {
                    rectangles.remove(i);
                }
            }
            repaint();
        } catch (IndexOutOfBoundsException | IllegalArgumentException a)
        {
            a.getStackTrace();
            a.printStackTrace();
            System.out.println("RectangleSlider method caught exception!");
        }
        catch(Exception e)
        {
            e.getStackTrace();
        }
    }

}
